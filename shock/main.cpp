//
//  main.cpp
//  shock
//
//  Created by 森川友裕 on 2021/08/17.
//

#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
const int n=100, L=1, step=4*n, t=1;
const double gamma=5./3.;
const double dx=(double)L/(double)n, dt=(double)t/(double)step;
const double C=10.,G=1.;
double capitalA (double s){
    double A=(s+4.*pow(s,3))/(1.+std::abs(s+4.*pow(s,3)));
    return A;
}
double capitalB (double s){
    double B=(pow(s,2)+2.)/(6.*pow(s,4)+2.*pow(s,2)+2.);
    return B;
}
class cap_M
{
public:
    double M[4],ULp[5],URp[5],ULm[5],URm[5],phiLp[4],phiRp[4],phiLm[4],phiRm[4],fluxp[4],fluxm[4],prefxp[4],prefxm[4],Mbox[4],rho, u, v, p, T,phib[4];//ULp[4]=p
    cap_M(double rho, double u, double v, double p){
        M[0]=rho;
        M[1]=rho*u;
        M[2]=rho*v;
        M[3]=(p/(gamma-1.)+rho*(u*u+v*v)/2.);
    }
    cap_M();
    void time_evo(){
        for(int i=0;i<4;i++){
            M[i]=Mbox[i]-dt/(2.*dx)*(fluxp[i]+prefxp[i]-fluxm[i]-prefxm[i]);
        }
        
    }
    void Filewriter(){
        rho =M[0];
        u=M[1]/M[0];
        v=M[2]/M[0];
        p=(gamma-1)*(M[3]-(pow(M[1],2)+pow(M[2],2))/(2.*M[0]));
        T=p/rho;
       

        
    }
    void MUSCLgetin(double Ul, double Ur, int i){
        URm[i]=Ul;
        ULp[i]=Ur;
    }
    void MUSCLgetout(double Ul, double Ur, int i){
        URp[i]=Ur;
        ULm[i]=Ul;
    }
    void phisavep(int ig){//プラス側
        phiLp[0]=ULp[1];
        phiRp[0]=URp[1];
        ULp[4]=(gamma-1)*(ULp[3]-((pow(ULp[1],2)+pow(ULp[2],2))/(2.*ULp[0])));//あやしい
        URp[4]=(gamma-1)*(URp[3]-((pow(URp[1],2)+pow(URp[2],2))/(2.*URp[0])));
        if(ULp[4]<0) {std::cout<<"マイナス発見 L plus phisave"<<ig<< std::endl;}
        if(URp[4]<0) {std::cout<<"マイナス発見 R plus phisave"<<ig<< std::endl;}
        phiLp[1]=pow(ULp[1],2)/ULp[0]+ULp[4];
        phiRp[1]=pow(URp[1],2)/URp[0]+URp[4];
        phiLp[2]=ULp[1]*ULp[2]/ULp[0];
        phiRp[2]=URp[1]*URp[2]/URp[0];
        phiLp[3]=(gamma/(1.-gamma)*ULp[4]+(pow(ULp[1],2)+pow(ULp[2],2))/(2.*ULp[0]))*ULp[1]/ULp[0];
        phiRp[3]=(gamma/(1.-gamma)*URp[4]+(pow(URp[1],2)+pow(URp[2],2))/(2.*URp[0]))*URp[1]/URp[0];
    }
    void phisavem(int ig){//マイナス側
        phiLm[0]=ULm[1];
        phiRm[0]=URm[1];
        ULm[4]=(gamma-1)*(ULm[3]-((pow(ULm[1],2)+pow(ULm[2],2))/(2.*ULm[0])));//p
        URm[4]=(gamma-1)*(URm[3]-((pow(URm[1],2)+pow(URm[2],2))/(2.*URm[0])));
        if(ULm[4]<0) {std::cout<<"マイナス発見 L minus phisave"<<ig<< std::endl;}
        if(URm[4]<0) {std::cout<<"マイナス発見 R minus phisave"<<ig<< std::endl;}
        phiLm[1]=pow(ULm[1],2)/ULm[0]+ULm[4];
        phiRm[1]=pow(URm[1],2)/URm[0]+URm[4];
        phiLm[2]=ULm[1]*ULm[2]/ULm[0];
        phiRm[2]=URm[1]*URm[2]/URm[0];
        phiLm[3]=(gamma/(1.-gamma)*ULm[4]+(pow(ULm[1],2)+pow(ULm[2],2))/(2.*ULm[0]))*ULm[1]/ULm[0];
        phiRm[3]=(gamma/(1.-gamma)*URm[4]+(pow(URm[1],2)+pow(URm[2],2))/(2.*URm[0]))*URm[1]/URm[0];
    }
    void FluxAandB(int ig){
        //プラス側
        double aL, aR, bL, bR, sL, sR, Fa[4], Fb[5], dL[4], dR[4], alpha;
        sR=URp[1]/URp[0]/std::sqrt(2.*URp[4]/URp[0]);
        sL=ULp[1]/ULp[0]/std::sqrt(2.*ULp[4]/ULp[0]);
        if(!std::isfinite(sL)) {std::cout<<"NAN発生sLp fluxAandB"<<ig<< std::endl;}
        if(!std::isfinite(sR)) {std::cout<<"NAN発生sRp fluxAandB"<<ig<< std::endl;}
        aL=capitalA(sL);
        aR=capitalA(sR);
        bL=std::sqrt(ULp[4]/ULp[0]/(2.*M_PI*G))*capitalB(sL);
        bR=std::sqrt(URp[4]/URp[0]/(2.*M_PI*G))*capitalB(sR);
        for (int i=0;i<4;i++){
            dL[i]=0.;
            dR[i]=0.;
        }
        dL[1]=ULp[0];
        dR[1]=URp[0];
        for(int i=0;i<4;i++){
            Fa[i]=(1.+aL)/2.*phiLp[i]+(1.-aR)/2.*phiRp[i]+bL*ULp[i]-bR*URp[i];
            Fb[i]=(1.+aL)/2.*ULp[i]+(1.-aR)/2.*URp[i]+bL*dL[i]-bR*dR[i];//ここはまだフラックスbではなく,Mをもとめている
        }
        //FluxbのM(phi)を求めている
        phib[0]=Fb[1];
        Fb[4]=(gamma-1)*(Fb[3]-((pow(Fb[1],2)+pow(Fb[2],2))/(2.*Fb[0])));
        phib[1]=pow(Fb[1],2)/Fb[0]+Fb[4];
        phib[2]=Fb[1]*Fb[2]/Fb[0];
        phib[3]=(gamma/(1.-gamma)*Fb[4]+(pow(Fb[1],2)+pow(Fb[2],2))/(2.*Fb[0]))*Fb[1]/Fb[0];
        
        alpha=1-std::exp(-C*std::abs(ULp[4]-URp[4])/(ULp[4]+URp[4]));
        if(!std::isfinite(alpha)) {std::cout<<"NAN発生alpha p influxAandB"<<ig<< std::endl;}
        for(int i=0;i<4;i++){
            fluxp[i]=alpha*Fa[i]+(1.-alpha)*phib[i];
        }
        
        
        
        //マイナス側,文字使い回し
        sR=URm[1]/URm[0]/std::sqrt(2*URm[4]/URm[0]);
        sL=ULm[1]/ULm[0]/std::sqrt(2*ULm[4]/ULm[0]);
        if(!std::isfinite(sL)) {std::cout<<"NAN発生sLm fluxAandB"<<ig<< std::endl;}
        if(!std::isfinite(sR)) {std::cout<<"NAN発生sRm fluxAandB"<<ig<< std::endl;}
        aL=capitalA(sL);
        aR=capitalA(sR);
        bL=std::sqrt(ULm[4]/ULm[0]/(2.*M_PI*G))*capitalB(sL);
        bR=std::sqrt(URm[4]/URm[0]/(2.*M_PI*G))*capitalB(sR);
        for (int i=0;i<4;i++){
            dL[i]=0.;
            dR[i]=0.;
        }
        dL[1]=ULm[0];
        dR[1]=URm[0];
        for(int i=0;i<4;i++){
            Fa[i]=(1.+aL)/2.*phiLm[i]+(1.-aR)/2.*phiRm[i]+bL*ULm[i]-bR*URm[i];
            Fb[i]=(1.+aL)/2*ULm[i]+(1.-aR)/2.*URm[i]+bL*dL[i]-bR*dR[i];
        }
        phib[0]=Fb[1];
        Fb[4]=(gamma-1)*(Fb[3]-((pow(Fb[1],2)+pow(Fb[2],2))/(2.*Fb[0])));
        phib[1]=pow(Fb[1],2)/Fb[0]+Fb[4];
        phib[2]=Fb[1]*Fb[2]/Fb[0];
        phib[3]=(gamma/(1.-gamma)*Fb[4]+(pow(Fb[1],2)+pow(Fb[2],2))/(2.*Fb[0]))*Fb[1]/Fb[0];
        alpha=1-std::exp(-C*std::abs(ULm[4]-URm[4])/(ULm[4]+URm[4]));
        if(!std::isfinite(alpha)) {std::cout<<"NAN発生alpham influxAandB"<<ig<< std::endl;}
        for(int i=0;i<4;i++){
            fluxm[i]=alpha*Fa[i]+(1.-alpha)*phib[i];
        }
        
    }
    void predictor(){
        for(int i=0;i<4;i++){
            Mbox[i]=M[i];//本当のMを避難
            M[i]=M[i]-dt/dx*(fluxp[i]-fluxm[i]);//ここで予測子のMを直接Mに入れてしまう.
        }
    }
    void preFlux(int ig){
        //プラス側
        double aL, aR, bL, bR, sL, sR, Fa[4], Fb[5], dL[4], dR[4], alpha;
        sR=URp[1]/URp[0]/std::sqrt(2.*URp[4]/URp[0]);
        sL=ULp[1]/ULp[0]/std::sqrt(2.*ULp[4]/ULp[0]);
        if(!std::isfinite(sR)) {std::cout<<"NAN発生 sRp preflux"<<ig<< std::endl;}
        if(!std::isfinite(sL)) {std::cout<<"NAN発生 sLp preflux"<<ig<< std::endl;}
        aL=capitalA(sL);
        aR=capitalA(sR);
        bL=std::sqrt(ULp[4]/ULp[0]/(2.*M_PI*G))*capitalB(sL);
        bR=std::sqrt(URp[4]/URp[0]/(2.*M_PI*G))*capitalB(sR);
        for (int i=0;i<4;i++){
            dL[i]=0.;
            dR[i]=0.;
        }
        dL[1]=ULp[0];
        dR[1]=URp[0];
        for(int i=0;i<4;i++){
            Fa[i]=(1.+aL)/2.*phiLp[i]+(1.-aR)/2.*phiRp[i]+bL*ULp[i]-bR*URp[i];
            Fb[i]=(1.+aL)/2*ULp[i]+(1.-aR)/2.*URp[i]+bL*dL[i]-bR*dR[i];
        }
        phib[0]=Fb[1];
        Fb[4]=(gamma-1)*(Fb[3]-((pow(Fb[1],2)+pow(Fb[2],2))/(2.*Fb[0])));
        phib[1]=pow(Fb[1],2)/Fb[0]+Fb[4];
        phib[2]=Fb[1]*Fb[2]/Fb[0];
        phib[3]=(gamma/(1.-gamma)*Fb[4]+(pow(Fb[1],2)+pow(Fb[2],2))/(2.*Fb[0]))*Fb[1]/Fb[0];
        alpha=1.-std::exp(-C*std::abs(ULp[4]-URp[4])/(ULp[4]+URp[4]));
        if(!std::isfinite(alpha)) {std::cout<<"NAN発生alpha plus preflux"<<ig<< std::endl;}
        for(int i=0;i<4;i++){
            prefxp[i]=alpha*Fa[i]+(1.-alpha)*phib[i];
        }
        
        
        
        //マイナス側,文字使い回し
        sR=URm[1]/URm[0]/std::sqrt(2.*URm[4]/URm[0]);
        sL=ULm[1]/ULm[0]/std::sqrt(2.*ULm[4]/ULm[0]);
        if(!std::isfinite(sR)) {std::cout<<"NAN発生 sRm preflux"<<ig<< std::endl;}
        if(!std::isfinite(sL)) {std::cout<<"NAN発生 sLm preflux"<<ig<< std::endl;}
        aL=capitalA(sL);
        aR=capitalA(sR);
        bL=std::sqrt(ULm[4]/ULm[0]/(2.*M_PI*G))*capitalB(sL);
        bR=std::sqrt(URm[4]/URm[0]/(2.*M_PI*G))*capitalB(sR);
        for (int i=0;i<4;i++){
            dL[i]=0.;
            dR[i]=0.;
        }
        dL[1]=ULm[0];
        dR[1]=URm[0];
        for(int i=0;i<4;i++){
            Fa[i]=(1.+aL)/2.*phiLm[i]+(1.-aR)/2.*phiRm[i]+bL*ULm[i]-bR*URm[i];
            Fb[i]=(1.+aL)/2*ULm[i]+(1.-aR)/2.*URm[i]+bL*dL[i]-bR*dR[i];
        }
        phib[0]=Fb[1];
        Fb[4]=(gamma-1)*(Fb[3]-((pow(Fb[1],2)+pow(Fb[2],2))/(2.*Fb[0])));
        phib[1]=pow(Fb[1],2)/Fb[0]+Fb[4];
        phib[2]=Fb[1]*Fb[2]/Fb[0];
        phib[3]=(gamma/(1.-gamma)*Fb[4]+(pow(Fb[1],2)+pow(Fb[2],2))/(2.*Fb[0]))*Fb[1]/Fb[0];
        alpha=1.-std::exp(-C*std::abs(ULm[4]-URm[4])/(ULm[4]+URm[4]));
        if(!std::isfinite(alpha)) {std::cout<<"NAN発生alpha minus　preflux"<<ig<< std::endl;}
        for(int i=0;i<4;i++){
            prefxm[i]=alpha*Fa[i]+(1.-alpha)*phib[i];
        }
    }
    

};
//右と左と真ん中のセルを受け取るそして真ん中のセルにML、MRを返す.
void MUSCL(cap_M Up, cap_M Um, cap_M &U, int ig, double times){
    double S;
        for(int i=0; i<4; i++){
            if (((Up.M[i]-U.M[i])*(U.M[i]-Um.M[i]))>0) S=2.*(Up.M[i]-U.M[i])*(U.M[i]-Um.M[i])/(dx*(Up.M[i]-Um.M[i]));
            //else if (((Up.M[i]-U.M[i])*(U.M[i]-Um.M[i]))>0) S=0.0;
            else S=0.0;
            if (S!=0.) std::cout <<"ゼロじゃないですね　i="<<ig<<"　times="<<times<<" 閾値"<<(Up.M[i]-U.M[i])*(U.M[i]-Um.M[i])<<std::endl;
            double UL=U.M[i]-S*0.5*dx;
            double UR=U.M[i]+S*0.5*dx;
            //if (UL<0) std::cout<<"マイナス発見 UL inMUSCL i="<<i<<" ig="<<ig<<" times="<<times<<std::endl;
            //if (UR<0) std::cout<<"マイナス発見 UR inMUSCL i="<<i<<" ig="<<ig<<" times="<<times<<std::endl;
            U.MUSCLgetin(UL, UR, i);
        }
}
void MUSCLtrans(cap_M Up, cap_M Um, cap_M &U){
    for (int i=0;i<4;i++){
        U.MUSCLgetout(Um.ULp[i], Up.URm[i],i);
    }
}



int main(int argc, const char * argv[]) {
    // insert code here...
    int frag=0;
    std::string my_pwd=getenv("HOME");
    std::string myfile=my_pwd+"/shockdata.txt";
    std::string myfile1=my_pwd+"/shockdatar.txt";
    std::string myfile2=my_pwd+"/shockdatau.txt";
    std::string myfile3=my_pwd+"/shockdatav.txt";
    std::string myfile4=my_pwd+"/shockdatap.txt";
    std::string myfile5=my_pwd+"/shockdataT.txt";
    std::ofstream output(myfile);
    std::ofstream output1(myfile1);
    std::ofstream output2(myfile2);
    std::ofstream output3(myfile3);
    std::ofstream output4(myfile4);
    std::ofstream output5(myfile5);
    std::vector<cap_M> M;
    //initialize////////////////////////////////////////////////////////
    //x<0.5//p=1,rho=1,u=0,v=0,T=1,//x>0.5//p=1,rho=1,u=3,v=0,T=1
    double rho=1.,u=0.0,v=0.,p=1.;
    for(int i=0;i<(n/2);i++){
        M.emplace_back(rho, u, v, p);//rho,u,v,p
    }
    //ランキンユゴニオ
    rho=4.*pow(u,2)/(pow(u,2)+3.);
    p=(5.+pow(u,2)-1.)/4;
    u=std::sqrt(1.-4.*(pow(u,2)-1)/(5.*pow(u,2)-1));
    for(int i=n/2;i<n;i++){
        M.emplace_back(0.125, 0., 0., 0.1);
    }
     
    /*
    for(int i=0;i<(n/2);i++){
        M.emplace_back(rho, u, v, p);//rho,u,v,p
    }
    rho=4.*pow(u,2)/(pow(u,2)+3.);
    p=(5.+pow(u,2)-1.)/4;
    u=std::sqrt(1.-4.*(pow(u,2)-1)/(5.*pow(u,2)-1));
    for(int i=0;i<10;i++)M.emplace_back(rho, u, v, p);
    
    rho=1.;
    u=1.5;v=0.;p=1.;
    for(int i=10;i<(n/2);i++){
        M.emplace_back(rho, u, v, p);//rho,u,v,p
    }
     */
    double times=0.;
    for (int i=0;i<n;i++){
        M[i].Filewriter();
    }
    for (int i=0;i<n;i++){

        output <<times<<" "<<dx*i<<" "<<M[i].rho<<" " <<M[i].u<<" " <<M[i].v<<" " <<M[i].p<<" " <<M[i].T<< std::endl;
        //output1 <<times<<" "<<dx*i<<" "<<M[i].rho<< std::endl;
        //output2 <<times<<" "<<dx*i<<" "<<M[i].u<<std::endl;
        //output3 <<times<<" "<<dx*i<<" "<<M[i].v<< std::endl;
        //output4 <<times<<" "<<dx*i<<" "<<M[i].p<<std::endl;
        //output5 <<times<<" "<<dx*i<<" "<<M[i].T<< std::endl;
       
    }
    
    do{
    //end///////////////////////////////////////////////////////////////
    MUSCL(M[1], M[0], M[0],0,times);
    MUSCL(M[n-1], M[n-2], M[n-1],n-1,times);//境界条件は固定
    for (int i=1;i<n-1;i++){
        MUSCL(M[i+1], M[i-1], M[i],i,times);
    }
    
    for (int i=1;i<n-1;i++){
        MUSCLtrans(M[i+1], M[i-1], M[i]);
    }

    for (int i=1;i<n-1;i++){
        M[i].phisavem(i);
        M[i].phisavep(i);
    }
    for (int i=1;i<n-1;i++){
        M[i].FluxAandB(i);
    }
    for (int i=1;i<n-1;i++){
        M[i].predictor();
    }
    ///二週目
    MUSCL(M[1], M[0], M[0],0,times+1);
    MUSCL(M[n-1], M[n-2], M[n-1],n-1,times);//境界条件は固定
    for (int i=1;i<n-1;i++){
        MUSCL(M[i+1], M[i-1], M[i],i,times);
    }
    
    for (int i=1;i<n-1;i++){
        MUSCLtrans(M[i+1], M[i-1], M[i]);
    }
    
    for (int i=1;i<n-1;i++){
        M[i].phisavem(i);
        M[i].phisavep(i);
    }
    for (int i=1;i<n-1;i++){
        M[i].preFlux(i);
    }
    for (int i=1;i<n-1;i++){
        M[i].time_evo();
    }
        times+=dt;
        for (int i=0;i<n;i++){
            M[i].Filewriter();
        }
        for (int i=0;i<n;i++){
            if(!std::isfinite(M[i].rho)) {std::cout<<"NAN発生rho"<<std::endl;frag=1;}
            if(!std::isfinite(M[i].u)) {std::cout<<"NAN発生u"<<std::endl;frag=1;}
            if(!std::isfinite(M[i].v)) {std::cout<<"NAN発生v"<<std::endl;frag=1;}
            if(!std::isfinite(M[i].p)) {std::cout<<"NAN発生p"<<std::endl;frag=1;}
            if(!std::isfinite(M[i].T)) {std::cout<<"NAN発生T"<<std::endl;frag=1;}
            output <<times<<" "<<dx*i<<" "<<M[i].rho<<" " <<M[i].u<<" " <<M[i].v<<" " <<M[i].p<<" " <<M[i].T<< std::endl;
        }
        if(frag==1) break;
        //std::ofstream output1(myfile1);
        std::ofstream output2(myfile2);
        std::ofstream output3(myfile3);
        std::ofstream output4(myfile4);
        std::ofstream output5(myfile5);
    for (int i=0;i<n;i++){
        
        
        if (times==0.005) output1<<dx*i<<" "<<M[i].rho<< std::endl;
        else std::cout <<times<<std::endl;
        output2 <<dx*i<<" "<<M[i].u<<std::endl;
        output3 <<dx*i<<" "<<M[i].v<< std::endl;
        output4 <<dx*i<<" "<<M[i].p<<std::endl;
        output5 <<dx*i<<" "<<M[i].T<< std::endl;
        //output <<times<<" "<<dx*i<<" "<<M[i].M[0]<<" " <<M[i].M[1]<<" " <<M[i].M[2]<<" " <<M[i].M[3]<<" " << std::endl;
    }
        
    }while(times<t);
    output.close();
    output1.close();
    output2.close();
    output3.close();
    output4.close();
    output5.close();
    
    

    return 0;
    };
